import { Component } from '@angular/core';

enum Colores { rojo, amarillo, verde }

@Component({
  selector: 'traf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public semaforos = {
    vehiculos: {
      color: Colores.verde,
      texto: 'Pueden circular'
    },
    peatones: {
      color: Colores.rojo,
      texto: 'Deben detenerse'
    }
  };
  public semaforosFuncionando = false;
  public intervalo;

  toggleSemaforo() {
    this.semaforosFuncionando = !this.semaforosFuncionando;
    if (this.semaforosFuncionando) {
      this.iniciaCiclo();
    } else {
      this.paraCiclo();
    }
  }

  avanzaSemaforo(tipo: string) {
    const semaforo = this.semaforos[tipo];
    switch (semaforo.color) {
      case Colores.verde:
        semaforo.color = tipo === 'vehiculos' ? Colores.amarillo : Colores.rojo;
        semaforo.texto = tipo === 'vehiculos' ? 'Precaución' : 'Deben detenerse';
        break;
      case Colores.amarillo:
        semaforo.color = Colores.rojo;
        semaforo.texto = 'Deben detenerse';
        this.avanzaSemaforo('peatones');
        break;
      case Colores.rojo:
        semaforo.color = Colores.verde;
        semaforo.texto = 'Pueden circular';
        if (tipo === 'vehiculos') { this.avanzaSemaforo('peatones'); }
    }
  }

  iniciaCiclo() {
    this.intervalo = setInterval(() => {
      this.avanzaSemaforo('vehiculos');
    }, 1000);
  }

  paraCiclo() {
    clearInterval(this.intervalo);
  }

  esColorSemaforo(tipo: string, color: string): boolean {
    return this.semaforos[tipo].color === Colores[color];
  }
}
